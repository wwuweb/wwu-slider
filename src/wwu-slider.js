/**
 * @file source code for Western Slider module.
 * @author Nigel Packer
 *
 * @namespace wwu
 */
var wwu = (function($, wwu, window, document, undefined) {

  'use strict';

  /**
   * @constant
   * @type {string}
   * @default
   */
  var PLUGIN = 'wwu-slider';

  /**
   * Calculate the width of the slider.
   * @function calc_width
   * @param {number} item_count - Total number of elements in the slider.
   * @param {number} items_to_show - Number of elements to show in the slider
   *   at one time.
   * @return {number}
   */
  function calc_width(item_count, items_to_show) {
    return item_count / items_to_show * 100 + '%';
  }

  /**
   * Calculate the left margin of the slider to set the scroll position.
   * @function calc_margin
   * @param {number} first_index - The index of the first slider item.
   * @param {number} items_to_show - Number of elements to show in the slider
   *   at one time.
   * @return {number}
   */
  function calc_margin(first_index, items_to_show) {
    return Math.ceil(first_index / items_to_show) * 100 + '%';
  }

  /**
   * A jQuery slider module. Displays an unordered list of elements as an
   * animated, horizontally scrolling slideshow.
   *
   * The slider is a styled unordered list displayed as a table, with each
   * element displayed as a table cell. This automatically ensures that items
   * have equal widths without scripting. The currently visible item is
   * determined by sliding the entire aboslutely-positioned element back and
   * forth using the left margin. Percentages are used for all dimensions, which
   * allows the slider to scale with the browser resolution.
   *
   * The slider keeps track of state by assigning an index to every item using
   * jQuery.data(). The indexing scheme is 0-based, except that 0 is always the
   * index of the first visible element. Items scrolled to the left have
   * negative indices while items scrolled to the right have positive indices.
   * This makes it programatically possible to determine the current position of
   * the slider from only the data stored in the DOM.
   *
   * Responsive behavior is provided by enquire.js, which brings CSS3 media
   * queries to JavaScript. This allows the number of items in the slider to be
   * customized based on the resolution of the browser.
   *
   * @example <caption>This will create a slider that shows three items when the
   * browser width is 800px or above, two items below 800px, and 1 item below
   * 40px. Note that the slider may break at resolutions not covered by a
   * breakpoint rule.</caption>
   * $('#slider').wwuSlider({
   *   'screen and (min-width: 800px)': 3,
   *   'screen and (min-width: 400px) and (max-width: 799px)': 2,
   *   'screen and (max-width: 399px)': 1
   * });
   *
   * @module wwu/Slider
   *
   * @requires jQuery
   * @requires enquire
   *
   * Creates a new wwu Slider instance.
   * @class
   * @param {string} selector - The element to convert into a slider.
   * @param {Object} breakpoints - Hash of media queries that map to the number
   *   of items to display in the slider at each resolution range.
   */
  wwu.Slider = function(selector, breakpoints) {

    /**
     * The slider element itself, as passed in from the constructor by the user.
     * @member {JQuery} $slider
     */
    var $slider = $(selector);

    /**
     * The number of elements in the slider.
     * @member {number} count
     */
    var count = $slider.children().length;

    /**
     * Collection of the individual slider items.
     * @member {JQuery} $items
     */
    var $items = $slider.find('li');

    /**
     * Pointer to the first element in the slider.
     * @member {JQuery} $first_item
     */
    var $first_item = $items.first();

    /**
     * Pointer to the last element in the slider.
     * @member {JQuery} $last_item
     */
    var $last_item = $items.last();

    /**
     * The number of items to show in the slider at once.
     * @member {number} display
     */
    var display;

    /**
     * The event types on which to trigger input to the slider controls.
     * @member {string} events
     */
    var events = 'click keyUp';

    /**
     * A wrapper element added around the base slider markup to determine height
     * and contain slider controls.
     * @member {JQuery} $wrapper
     */
    var $wrapper;

    /**
     * Control to advance the currently visible slider elements.
     * @member {JQuery} $next_control
     */
    var $next_control;

    /**
     * Control to reverse the currently visible slider elements.
     * @member {JQuery} $prev_control
     */
    var $prev_control;

    init();

    /**
     * Attach the event listeners to the slider controls.
     * @method bind_controls
     */
    function bind_controls() {
      $next_control.on(events, next);
      $prev_control.on(events, prev);
    }

    /**
     * Perform initial setup tasks for the slider.
     *
     * Generate the wrapper and control markup and place these in the needed
     * lcoations in the DOM.
     * @method init
     */
    function init() {
      $wrapper = $('<div></div>', {
        'class': PLUGIN + '-slider-inner-wrapper',
        'css' : {
          'position': 'relative'
        }
      });

      $next_control = $('<button></button>', {
        'class': PLUGIN + '-control ' + PLUGIN + '-control-next',
        'aria-role': 'button'
      });

      $prev_control = $('<button></button>', {
        'class': PLUGIN + '-control ' + PLUGIN + '-control-prev',
        'aria-role': 'button'
      });

      $prev_control.appendTo($wrapper);
      $next_control.appendTo($wrapper);

      // Move the slider into the wrapper
      $wrapper.insertBefore($slider);
      $slider.appendTo($wrapper);

      $slider.css({
        'display': 'table',
        'left': '0',
        'list-style': 'none',
        'margin': '0',
        'padding': '0',
        'position': 'absolute',
        'table-layout': 'fixed',
        'top': '0'
      });

      $items.css({
        'box-sizing': 'content-box',
        'display': 'table-cell',
        'padding-right': '5px',
        'padding-left': '5px',
      });

      for (var breakpoint in breakpoints) {
        enquire.register(breakpoint, make_breakpoint_handler(breakpoint));
      }

      bind_controls();
    }

    /**
     * Generate a handler to call when the given breakpoint is triggered. The
     * value of the breakpoint is bound by the closure.
     * @method make_breakpoint_handler
     * @param {string} breakpoint - The breakpoint rule as key to the
     *   breakpoints hash.
     * @return {Function}
     */
    function make_breakpoint_handler(breakpoint) {
      return function () {
        display = breakpoints[breakpoint];

        reset();
        update();

        $slider.css({
          'width': calc_width(count, display),
          'margin-left': calc_margin($first_item.data('slider_item_index'), display)
        });
      };
    }

    /**
     * Update the scroll position of the slider.
     * @method scroll
     */
    function scroll() {
      unbind_controls();
      $slider.animate({
        'margin-left': calc_margin($first_item.data('slider_item_index'), display)
      }, {
        complete: bind_controls
      });
    }

    /**
     * Advance the visible elements in the slider.
     * @method next
     */
    function next() {
      update(-display);
      scroll();
    }

    /**
     * Reverse the visible elements in the slider.
     * @method prev
     */
    function prev() {
      update(display);
      scroll();
    }

    /**
     * Reset the slider scroll position.
     * @method reset
     */
    function reset() {
      $items.each(function (index, element) {
        $(element).removeData('slider_item_index');
      });
    }

    /**
     * Remove event listeners from the slider controls.
     *
     * This may be used to debounce control input.
     * @method unbind_controls
     */
    function unbind_controls() {
      $next_control.off();
      $prev_control.off();
    }

    /**
     * Update the slider element data.
     *
     * Updates the jQuery data containing the index of each element and sets the
     * accessibility attributes appropriately. Updates the state of the controls
     * based on the current position of the slider.
     * @method update
     * @param {number} increment - The number of elements to advance; defaults
     *   to 0.
     */
    function update(increment) {
      var disabled;

      increment = (typeof increment !== 'undefined') ? increment : 0;

      $items.each(function (index, element) {
        var $element = $(element);
        var slider_item_index = $element.data('slider_item_index');

        if (slider_item_index === undefined) {
          slider_item_index = index;
        } else {
          slider_item_index += increment;
        }

        if (slider_item_index >= 0 && slider_item_index < display) {
          $element.attr('aria-hidden', false);
          $element.find('a').removeAttr('tabindex');
        } else {
          $element.attr('aria-hidden', true);
          $element.find('a').attr('tabindex', -1);
        }

        $element.data('slider_item_index', slider_item_index);
      });

      disabled = $first_item.data('slider_item_index') >= 0;

      $prev_control.prop('disabled', disabled);
      $prev_control.attr('aria-disabled', disabled);

      disabled = $last_item.data('slider_item_index') < display;

      $next_control.prop('disabled', disabled);
      $next_control.attr('aria-disabled', disabled);
    }
  };

  $.fn.wwuSlider = function(options) {
    return this.each(function (index, element) {
      var instance = $.data(element, PLUGIN);

      if (instance) {
        instance[options]();
      }
      else {
        new wwu.Slider(element, options);
      }
    });
  };

  return wwu;

})(jQuery, wwu || {}, this, this.document);
